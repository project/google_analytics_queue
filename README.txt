Module: Google Analytics PHP Queue
==================================

Description
===========
Sometimes there is a need to track PHP events with Google Analytics. An example
is when your user is completing a form on your site, if some of your users are
having trouble completing some of the fields, or forgetting to fill them in,
the googleanalytics module does not track this out of the box.

This module provides an API so that custom modules can easily set events to be
tracked on the next page view for the particular user in question.

The Google Analytics PHP Queue module stores these events in the user's session,
so next time they visit a page the event is written out as Javascript so that
Google Analytics can record it.


Requirements
============
Google Analytics module is installed and correctly configured


Installation
============
Copy the 'google_analytics_queue' module directory in to your Drupal
sites/all/modules directory as usual.


Usage
=====
When you wish to track an event, in PHP call for example:

google_analytics_queue_add(array('_trackEvent', 'User Account', 'Registration', 'Successful'));

The above code will produce a googleanalytics event on the very next page load
for that particular user.
